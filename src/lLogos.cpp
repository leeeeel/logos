#include "lLogos.hpp"

#include "modifiers/level.hpp"
#include "modifiers/message.hpp"

//std
#include <iostream>

namespace logos
{
    void LLogos::log(const std::string &message)
    {
        log(message, m_level);
    }

    void LLogos::log(const std::string &message, const modifier::LevelCode level)
    {
        if (m_level <= level)
        {
            m_mutex.lock();
            std::cout
                << modifier::Level(level)
                << " "
                << modifier::Message(message)
                << "\n";
            m_mutex.unlock();
        }
    }

    void LLogos::setLevel(const modifier::LevelCode level)
    {
        m_level = level;
    }

    void LLogos::debug(const std::string &message)
    {
        log(message, modifier::LevelCode::DEBUG);
    }
    void LLogos::info(const std::string &message)
    {
        log(message, modifier::LevelCode::INFO);
    }
    void LLogos::warning(const std::string &message)
    {
        log(message, modifier::LevelCode::WARNING);
    }
    void LLogos::error(const std::string &message)
    {
        log(message, modifier::LevelCode::ERROR);
    }
    void LLogos::critical(const std::string &message)
    {
        log(message, modifier::LevelCode::CRITICAL);
    }
} // namespace logos