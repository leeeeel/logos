#pragma once

#include "lLogos.hpp"
#include "modifiers/level.hpp"

// std
#include <mutex>
#include <string>

namespace logos
{
    class LttmLogos : public LLogos // Level Time Thread Message logos
    {
    public:
        static void log(const std::string &message, const modifier::LevelCode level);
    };
} // namespace logos