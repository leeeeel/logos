#pragma once

#include "modifiers/modifier.hpp"
#include "modifiers/level.hpp"

// std
#include <mutex>
#include <string>

namespace logos
{
    class LLogos
    {
    public:
        static void log(const std::string &message);
        static void log(const std::string &message, const modifier::LevelCode level);

        static void setLevel(const modifier::LevelCode level);

        static void debug(const std::string &message);
        static void info(const std::string &message);
        static void warning(const std::string &message);
        static void error(const std::string &message);
        static void critical(const std::string &message);

    protected:
        static inline std::mutex m_mutex;
        static inline modifier::LevelCode m_level = modifier::LevelCode::INFO;
    };
} // namespace logos