#include "time.hpp"

namespace logos
{
    namespace modifier
    {
        std::string Time::output() const
        {
            char buffer[21];
            std::time_t epochTime = std::time(nullptr);
            std::strftime(buffer, 21, "%Y-%m-%d %H:%M:%S", std::localtime(&epochTime));
            std::string time = std::string(buffer);

            return time;
        }
    } // namespace modifier
} // namespace logos