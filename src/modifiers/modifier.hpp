#pragma once

// std
#include <ostream>
#include <string>

namespace logos
{
    namespace modifier
    {
        class Modifier
        {
        public:
            virtual std::string output() const = 0;
            friend std::ostream &operator<<(std::ostream &os, const Modifier &mod)
            {
                return os << mod.output();
            }
            friend std::string operator+(const std::string &value, const Modifier &mod)
            {
                return value + mod.output();
            }
            friend std::string operator+(const Modifier &mod, const std::string &value)
            {
                return mod.output() + value;
            }
        };
    } // namespace modifier
} // namespace logos