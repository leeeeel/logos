#include "thread.hpp"

// std
#include <thread>

namespace logos
{
    namespace modifier
    {
        std::string Thread::output() const
        {
            std::hash<std::thread::id> hasher;
            std::thread::id threadId = std::this_thread::get_id();
            return std::to_string(hasher(threadId)).substr(0, 8);
        }
    } // namespace modifier
} // namespace logos