#pragma once

#include "color.hpp"
#include "modifier.hpp"

// std
#include <string>

namespace logos
{
    namespace modifier
    {
        enum LevelCode : short
        {
            NOTSET,
            DEBUG,
            INFO,
            WARNING,
            ERROR,
            CRITICAL
        };
        const inline char *levelValues[6] = {
            "NOTSET",
            "DEBUG",
            "INFO",
            "WARNING",
            "ERROR",
            "CRITICAL",
        };
        const inline ColorCode levelColors[6] = {
            FG_CYAN,
            FG_BLUE,
            FG_GREEN,
            FG_YELLOW,
            FG_RED,
            FG_MAGENTA,
        };

        class Level : public Modifier
        {
        public:
            Level(const std::string name, Color color) : m_name{name}, m_color(color) {}
            Level(const LevelCode code) : m_name{levelValues[code]}, m_color(levelColors[code]) {}

            virtual std::string output() const override;

        private:
            std::string m_name;
            Color m_color;
        };
    } // namespace modifier
} // namespace logos
