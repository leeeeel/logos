#pragma once

#include "modifier.hpp"

// std
#include <string>

namespace logos
{
    namespace modifier
    {
        class Message : public Modifier
        {
        public:
            Message(const std::string &message) : m_message(message) {}

            virtual std::string output() const override;

        private:
            std::string m_message;
        };
    } // namespace modifier
} // namespace logos