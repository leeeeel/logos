#pragma once

#include "modifier.hpp"

// std
#include <string>

namespace logos
{
    namespace modifier
    {
        class Thread : public Modifier
        {
        public:
            Thread() {}

            virtual std::string output() const override;
        };
    } // namespace modifier
} // namespace logos