#pragma once

#include "modifier.hpp"

// std
#include <string>

namespace logos
{
    namespace modifier
    {
        enum ColorCode
        {
            FG_RED = 31,
            FG_GREEN = 32,
            FG_YELLOW = 33,
            FG_BLUE = 34,
            FG_MAGENTA = 35,
            FG_CYAN = 36,
            FG_WHITE = 37,
            FG_DEFAULT = 39,
        };

        class Color : public Modifier
        {
        public:
            Color(ColorCode code) : m_code(code) {}

            virtual std::string output() const override;

        private:
            ColorCode m_code;
        };
    } // namespace modifier
} // namespace logos