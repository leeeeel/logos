#include "message.hpp"

namespace logos
{
    namespace modifier
    {
        std::string Message::output() const
        {
            return m_message;
        }
    } // namespace modifier
} // namespace logos