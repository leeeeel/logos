#pragma once

#include "modifier.hpp"

// std
#include <ctime>
#include <string>

namespace logos
{
    namespace modifier
    {
        class Time : public Modifier
        {
        public:
            Time() {}

            virtual std::string output() const override;
        };
    } // namespace modifier
} // namespace logos