#include "color.hpp"

namespace logos
{
    namespace modifier
    {
        std::string Color::output() const
        {
            return "\033[" + std::to_string(m_code) + "m";
        }
    } // namespace modifier
} // namespace logos