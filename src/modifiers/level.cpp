#include "level.hpp"

namespace logos
{
    namespace modifier
    {
        std::string Level::output() const
        {
            return m_color + m_name + Color(FG_DEFAULT);
        }
    } // namespace modifier
} // namespace logos
