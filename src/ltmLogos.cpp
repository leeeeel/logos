#include "ltmLogos.hpp"

#include "modifiers/level.hpp"
#include "modifiers/time.hpp"
#include "modifiers/message.hpp"

//std
#include <iostream>

namespace logos
{
    void LtmLogos::log(const std::string &message, const modifier::LevelCode level)
    {
        if (m_level <= level)
        {
            m_mutex.lock();
            std::cout
                << modifier::Level(level)
                << " "
                << modifier::Time()
                << modifier::Message(message)
                << "\n";
            m_mutex.unlock();
        }
    }
} // namespace logos