#pragma once

#include "lLogos.hpp"
#include "modifiers/level.hpp"

// std
#include <mutex>
#include <string>

namespace logos
{
    class LtmLogos : LLogos
    {
        static void log(const std::string &message, const modifier::LevelCode level);
    };
} // namespace logos