#include "lttmLogos.hpp"

#include "modifiers/level.hpp"
#include "modifiers/time.hpp"
#include "modifiers/thread.hpp"
#include "modifiers/message.hpp"

//std
#include <iostream>

namespace logos
{
    void LttmLogos::log(const std::string &message, const modifier::LevelCode level)
    {
        if (m_level <= level)
        {
            m_mutex.lock();
            std::cout
                << modifier::Level(level)
                << " "
                << modifier::Time()
                << " ["
                << modifier::Thread()
                << "] "
                << modifier::Message(message)
                << "\n";
            m_mutex.unlock();
        }
    }
} // namespace logos