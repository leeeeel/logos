#include "../../src/modifiers/thread.hpp"

#include "gtest/gtest.h"

// std
#include <thread>

namespace logos
{
    namespace test
    {
        TEST(ThreadTest, HandleThread)
        {
            std::hash<std::thread::id> hasher;
            std::thread::id threadId = std::this_thread::get_id();
            auto id = std::to_string(hasher(threadId)).substr(0, 8);

            EXPECT_EQ(id,
                      modifier::Thread().output());
        }
    } // namespace test
} // namespace logos
