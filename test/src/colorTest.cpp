#include "../../src/modifiers/color.hpp"

#include "gtest/gtest.h"

namespace logos
{
    namespace test
    {
        TEST(ColorTest, HandleDefaultColors)
        {
            EXPECT_EQ("\033[31m",
                      modifier::Color(modifier::ColorCode::FG_RED).output());
            EXPECT_EQ("\033[32m",
                      modifier::Color(modifier::ColorCode::FG_GREEN).output());
            EXPECT_EQ("\033[33m",
                      modifier::Color(modifier::ColorCode::FG_YELLOW).output());
            EXPECT_EQ("\033[34m",
                      modifier::Color(modifier::ColorCode::FG_BLUE).output());
            EXPECT_EQ("\033[35m",
                      modifier::Color(modifier::ColorCode::FG_MAGENTA).output());
            EXPECT_EQ("\033[36m",
                      modifier::Color(modifier::ColorCode::FG_CYAN).output());
            EXPECT_EQ("\033[37m",
                      modifier::Color(modifier::ColorCode::FG_WHITE).output());
            EXPECT_EQ("\033[39m",
                      modifier::Color(modifier::ColorCode::FG_DEFAULT).output());
        }
    } // namespace test
} // namespace logos
