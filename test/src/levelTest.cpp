#include "../../src/modifiers/level.hpp"

#include "gtest/gtest.h"

namespace logos
{
    namespace test
    {
        TEST(LevelTest, HandleDefaultLevels)
        {
            EXPECT_EQ("\033[36mNOTSET\033[39m",
                      modifier::Level(modifier::LevelCode::NOTSET).output());
            EXPECT_EQ("\033[34mDEBUG\033[39m",
                      modifier::Level(modifier::LevelCode::DEBUG).output());
            EXPECT_EQ("\033[32mINFO\033[39m",
                      modifier::Level(modifier::LevelCode::INFO).output());
            EXPECT_EQ("\033[33mWARNING\033[39m",
                      modifier::Level(modifier::LevelCode::WARNING).output());
            EXPECT_EQ("\033[31mERROR\033[39m",
                      modifier::Level(modifier::LevelCode::ERROR).output());
            EXPECT_EQ("\033[35mCRITICAL\033[39m",
                      modifier::Level(modifier::LevelCode::CRITICAL).output());
        }

        TEST(LevelTest, HandleCustomLevel)
        {
            EXPECT_EQ("\033[31mMOCK\033[39m",
                      modifier::Level("MOCK", modifier::ColorCode::FG_RED).output());
        }
    } // namespace test
} // namespace logos
