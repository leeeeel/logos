#include "../../src/modifiers/message.hpp"

#include "gtest/gtest.h"

namespace logos
{
    namespace test
    {
        TEST(MessageTest, HandleEmptyMessage)
        {
            EXPECT_EQ("",
                      modifier::Message("").output());
        }

        TEST(MessageTest, HandleMessage)
        {
            EXPECT_EQ("Mock message",
                      modifier::Message("Mock message").output());
        }
    } // namespace test
} // namespace logos
